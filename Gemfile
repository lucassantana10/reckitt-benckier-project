source 'https://rubygems.org'

ruby '2.3.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0.6'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

gem 'rest-client'

gem 'rack-rewrite', '~> 1.5.1'

gem "docraptor"
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

# Attachment
gem 'paperclip', '~> 4.3.6'
gem 'delayed_paperclip', '~> 2.10.0'
gem 'aws-sdk', '< 2.0'

#Decorations
gem 'draper', '~> 2.1.0'

# Soft Delete
gem 'paranoia', '~> 2.0'

# Monitoring
gem 'rollbar', '~> 2.11.4'

# Cron
gem 'crono', '~> 1.0.2'

# Server
gem 'puma', '~> 3.4.0'

# Nested
gem 'cocoon', '~> 1.2.9'

#=========

gem 'kaminari', '~> 0.16.3'

gem 'axlsx', git: 'https://github.com/randym/axlsx.git'

gem 'therubyracer' # Ruby

gem 'pg'

gem 'cancancan', '~> 1.9'

gem 'devise', '~> 3.4.1'

gem "bower-rails", "~> 0.9.1"

gem 'bootstrap-sass', '~> 3.3.4'

gem 'rails_12factor', group: :production

gem 'newrelic_rpm'

gem 'roo', '~> 2.3.1'

gem 'sidekiq', '~> 2.16.1'
gem 'sidekiq_status'
gem 'sinatra', require: false
gem 'slim'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "brakeman"
  gem 'byebug'
  gem 'rspec-rails', '~> 3.4'
  gem 'fog'
end


group :test do
  gem 'factory_girl_rails', '~> 4.0'
  gem 'shoulda-matchers', '~> 3.1'
end

group :production do
  gem 'dalli'
  gem "rack-timeout"
end
