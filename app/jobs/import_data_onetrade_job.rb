#
class ImportDataOnetradeJob < ActiveJob::Base
  queue_as :default

  def perform
    onetrade = Onetrade.joins('LEFT JOIN itineraries ON onetrade.visit_id = itineraries.visit_id')
              .where(itineraries: { visit_id: nil }).pluck(:visit_id).uniq
    process_data(onetrade)
  end

  private

  def process_data(onetrade)
    onetrade.map do |visit_id|
      Importers::ItineraryImporter.new(visit_id).process
    end
  end
end
