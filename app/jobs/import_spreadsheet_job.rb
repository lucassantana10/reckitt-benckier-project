class ImportSpreadsheetJob < ActiveJob::Base
  queue_as :default

  def perform(spreadsheet_id)
    spreadsheet = Spreadsheet.find(spreadsheet_id)

    process_spreadsheet(spreadsheet)
  end

  private

  def process_spreadsheet(spreadsheet)
    case spreadsheet.source_type
    when Spreadsheet::RULES
      Importers::RulesSpreadsheetImporter.new(spreadsheet).process
    when Spreadsheet::HIERARCHY
      Importers::HierarchySpreadsheetImporter.new(spreadsheet).process
    end
  end
end
