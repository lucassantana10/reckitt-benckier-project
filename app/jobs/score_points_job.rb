#
class ScorePointsJob < ActiveJob::Base
  queue_as :default

  def perform
    itineraries = Itinerary.all
    process_data(itineraries)
  end

  private

  def process_data(itineraries)
    itineraries.map do |itinerary|
      Queries::ScorePoints.new(itinerary.id).process
    end
  end
end
