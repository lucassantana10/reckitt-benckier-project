ActiveAdmin.register Itinerary do
  actions :all, except: [:edit]
  permit_params(
    :pos_tax_code, :retail, :shopping, :city, :region, :created_at,
    :postal_code, :state_code, :address, :district, :latitude, :longitude,
  )

  filter :pos_tax_code

  index do
    selectable_column
    id_column
    column :pos_tax_code
    column :execution_date_time
    column :retail
    column :city
    column :state_code
    actions
  end

  csv force_quotes: true, col_sep: ';', column_names: true do
    column :id
    column :pos_tax_code
    column :retail
    column :city
    column :state_code
  end

  show do
    columns do
      column do
        attributes_table do
          row :pos_tax_code
          row :retail
          row :shopping
          row :updated_at
          row :created_at
        end
      end
      column do
        attributes_table do
          row :city
          row :region
          row :state_code
          row :postal_code
          row :address
          row :number
          row :latitude
          row :longitude
        end
      end
    end
  end

  form do |_|
    inputs 'Detalhes' do
      input :pos_tax_code
      input :retail
      input :shopping
      input :created_at
      input :city
      input :region
      input :state_code
      input :postal_code
      input :address
      input :district
      input :latitude
      input :longitude
    end
    actions
  end
end
