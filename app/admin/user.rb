ActiveAdmin.register User do
  menu :label => "Usuário"
  permit_params :email, :name, :role_id, :password, :password_confirmation,
                resend_password: [], itinerary_ids: []

  index do
    selectable_column
    id_column
    column :name
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :name
  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  show do
    attributes_table do
      row :name
      row :email
      row :role
      row :updated_at
      row :created_at
    end
  end

  form do |f|
    f.inputs '' do
      f.input :name
      f.input :email
      f.input :role
      f.input :password
      f.input :password_confirmation
      f.input :itineraries, :input_html => { :size => 30 }, as: :select, :collection => Itinerary.all.order(:store).map{|u| ["#{u.store}, #{u.city}", u.id]}
    end
    f.actions
  end
end
