ActiveAdmin.register Spreadsheet do
  actions :all, except: [:edit, :destroy]
  menu label: 'Uploads'
  permit_params(:file, :source_type)

  controller do
    def create
      @spreadsheet = Spreadsheet.create(permitted_params[:spreadsheet])

      if @spreadsheet.persisted?
        ImportSpreadsheetJob.perform_later(@spreadsheet.id)
        redirect_to admin_spreadsheet_url(@spreadsheet)
      end
    end
  end

  filter :success
  filter :processed_at
  filter :source_type
  filter :updated_at
  filter :created_at

  index do
    id_column
    column :success
    column :processed_at
    column(:source_type) { |s| Spreadsheet::SOURCE_TYPES.merge(Spreadsheet::SOURCE_TYPES)[s.source_type] }
    column :updated_at
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :success
      row :processed_at
      row(:source_type) { |s| Spreadsheet::SOURCE_TYPES.merge(Spreadsheet::SOURCE_TYPES)[s.source_type] }
      row :updated_at
      row :created_at
    end

    panel 'Erros' do
      if spreadsheet.issues.present?
        ul do
          spreadsheet.issues.each do |key, value|
            li do
              "#{key}: #{value}"
            end
          end
        end
      else
        h3 'Nenhum erro'
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :file, required: true
      f.input(
        :source_type,
        collection: Spreadsheet::SOURCE_TYPES.merge(Spreadsheet::SOURCE_TYPES).invert,
        required: true, include_blank: false)
    end

    f.actions
  end
end
