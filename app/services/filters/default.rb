module Filters
  class Default
    def initialize(filters = {}, current_ability = nil)
      @current_ability = current_ability
      @filters = filters
    end

    def process_default
      query_prepare
    end

    def process_store
      query_prepare_store
    end

    private

    def query_prepare
      response = []

      response << city_filter
      response << state_filter
      response << retail_filter
      response << segment_filter
      response << economic_group_filter
      response << area_manager_filter
      response << store_filter
      subcategory_filter.each do |row|
        response << row
      end
      response << manufacturer_filter
      response << brand_filter
      response << sku_filter
    end

    def query_prepare_store
      response = []

      response << city_filter
      response << state_filter
      response << retail_filter
      response << segment_filter
      response << economic_group_filter
      response << area_manager_filter
      response << store_filter
    end

    def city_filter
      data = []
      query(filter_params('itineraries', 'city')).each do |info|
        unless data.include?(info.city)
          data << info.city unless info.city.nil?
        end
      end

      { key: 'cities', values: data.sort_by { |_, v| v } }
    end

    def state_filter
      data = []
      query(filter_params('itineraries', 'state_code')).each do |info|
        unless data.include?(info.state_code)
          data << info.state_code unless info.state_code.nil?
        end
      end

      { key: 'states', values: data.sort_by { |_, v| v } }
    end

    def retail_filter
      data = []
      query(filter_params('itineraries', 'retail')).each do |info|
        unless data.include?(info.retail)
          data << info.retail unless info.retail.nil?
        end
      end

      { key: 'retails', values: data.sort_by { |_, v| v } }
    end

    def segment_filter
      data = []
      query(filter_params('itineraries', 'segment')).each do |info|
        unless data.include?(info.segment)
          data << info.segment unless info.segment.nil?
        end
      end

      { key: 'segments', values: data.sort_by { |_, v| v } }
    end

    def economic_group_filter
      data = []
      query(filter_params('itineraries', 'economic_group')).each do |info|
        unless data.include?(info.economic_group)
          data << info.economic_group unless info.economic_group.nil?
        end
      end

      { key: 'economic_groups', values: data.sort_by { |_, v| v } }
    end

    def area_manager_filter
      data = []
      query(filter_params('itineraries', 'area_manager')).each do |info|
        unless data.include?(info.area_manager)
          data << info.area_manager unless info.area_manager.nil?
        end
      end

      { key: 'area_managers', values: data.sort_by { |_, v| v } }
    end

    def store_filter
      data = []
      query(filter_params('itineraries', 'store')).each do |info|
        unless data.include?(info.store)
          data << info.store unless info.store.nil?
        end
      end

      { key: 'stores', values: data.sort_by { |_, v| v } }
    end

    def category_filter
      data = []
      query_item_features(filter_params('item_features', 'category')).each do |info|
        unless data.include?(info.category)
          data << info.category unless info.category.nil?
        end
      end

      { key: 'categories', values: data.sort_by { |_, v| v } }
    end

    def manufacturer_filter
      data = []
      query_item_features(filter_params('item_features', 'manufacturer')).each do |info|
        unless data.include?(info.manufacturer)
          data << info.manufacturer unless info.manufacturer.nil?
        end
      end

      { key: 'manufacturers', values: data.sort_by { |_, v| v } }
    end

    def brand_filter
      data = []
      query_item_features(filter_params('item_features', 'brand_2')).each do |info|
        unless data.include?(info.brand_2)
          data << info.brand_2 unless info.brand_2.nil?
        end
      end

      { key: 'brands', values: data.sort_by { |_, v| v } }
    end

    def sku_filter
      data = []
      query_item_features(filter_params('item_features', 'sku')).each do |info|
        unless data.include?(info.sku)
          data << info.sku unless info.sku.nil?
        end
      end

      { key: 'skus', values: data.sort_by { |_, v| v } }
    end

    def subcategory_filter
      data = {'bugsprays' => [], 'cleaners' => [], 'bleaches' => []}
      query_item_features(filter_params('item_features', 'subcategory')).each do |info|
        next if info.subcategory.nil?
        next if info.category.nil?
        if info.category == 'INSETICIDAS' && !data['bugsprays'].include?(info.subcategory)
          data['bugsprays'] << info.subcategory
        elsif info.category == 'LIMPADORES' && !data['cleaners'].include?(info.subcategory)
          data['cleaners'] << info.subcategory
        elsif info.category == 'ALVEJANTES' && !data['bleaches'].include?(info.subcategory)
          data['bleaches'] << info.subcategory
        end
      end

      [{ key: 'bugsprays', values: data['bugsprays'].sort_by { |_, v| v } },
       { key: 'cleaners', values: data['cleaners'].sort_by { |_, v| v } },
       { key: 'bleaches', values: data['bleaches'].sort_by { |_, v| v } }]
    end

    def filter_params(table, field)
      filter = {}
      if @filters.try(:[], table)
        filter[table] = @filters[table].reject { |k, _| k == field }
      end
      filter
    end

    def query(params)
      Rails.cache.fetch("query_filters_default#{params}", :expires_in => 12.hours) do
        QueryFilter.filter_relation(
          ::Itinerary.accessible_by(@current_ability)
                     .joins("LEFT JOIN items ON items.itinerary_id = itineraries.id")
                     .joins("LEFT JOIN item_features ON item_features.item_id = items.id"),
          params
        )
      end
    end

    def query_item_features(params)
      Rails.cache.fetch("query_item_features_filters_default#{params}", :expires_in => 12.hours) do
        QueryFilter.filter_relation(
          ::ItemFeature.accessible_by(@current_ability)
                       .joins("LEFT JOIN items ON items.id = item_features.item_id")
                       .joins("LEFT JOIN itineraries ON itineraries.id = items.itinerary_id"),
          params
        )
      end
    end
  end
end
