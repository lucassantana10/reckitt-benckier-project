module Queries
  class Picture
    def initialize(filters = {}, current_ability = nil, page = nil)
      @current_ability = current_ability
      @filters = filters
      @limit = 28
      @offset = (page.nil? || page.to_i == 1 ? 0 : ((page.to_i - 1) * @limit))
    end

    def process
      query_prepare
    end

    private

    def query_prepare
      array = []
      response = {}
      query(@filters).each do |info|
        array << info
      end
      response['content'] = array
      response['count'] = count(@filters)
      response
    end

    def count(params)
      QueryFilter.filter_relation(
        ::Picture.accessible_by(@current_ability)
                 .joins("LEFT JOIN itineraries ON itineraries.id = pictures.itinerary_id")
                 .joins("LEFT JOIN item_features ON item_features.item_id = pictures.item_id")
                 .where.not("pictures.category = 'FACHADA'"),
        params
      ).count('DISTINCT pictures.id')
    end

    def query(params)
      QueryFilter.filter_relation(
        ::Picture.accessible_by(@current_ability)
                 .joins("LEFT JOIN itineraries ON itineraries.id = pictures.itinerary_id")
                 .joins("LEFT JOIN item_features ON item_features.item_id = pictures.item_id")
                 .where.not("pictures.category = 'FACHADA'"),
        params
      ).limit(@limit).offset(@offset).select('DISTINCT ON (pictures.id) pictures.*, itineraries.store, item_features.sku')
    end
  end
end
