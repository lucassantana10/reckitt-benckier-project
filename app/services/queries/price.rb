module Queries
  class Price
    def initialize(filters = {}, current_ability = nil, page = nil)
      @current_ability = current_ability
      @filters = filters
    end

    def process
      query_prepare
    end

    def process_export(graph_type)
      case graph_type
      when 'price'
        export_query(@filters)
      end
    end

    private

    def query_prepare
      response = {}
      values = {}
      all_price = []
      weeks = {}

      query(@filters).each do |row|
        weeks[row[5]] ||= {}
        weeks[row[5]] = row[1]
      end

      query(@filters).each do |row|
        values[row[0]] ||= {}
        values[row[0]][row[5]] = row[2].to_f
        all_price << row[2].to_f
      end

      response['content'] = []
      values.each do |k, v|
        (weeks.keys - v.keys).each { |mk| v[mk] = 0 }
        response['content'] << { 'key' => k+"      ",
                                 'values' => v.to_a
                                              .sort_by{|k| k.first}
                                              .map
                                              .with_index { |w, i| [i, w.second] } }
      end

      mappings = {}
      weeks.map.with_index {|c, i| mappings[i] = c.second }
      all_price.sort!
      price_last = all_price.last.nil? ? 1 : all_price.last
      price_first = all_price.first.nil? ? 1 : all_price.first

      response['price_range'] = [price_first - (price_last * 0.10),
                                 price_last + (price_last * 0.20)]
      response['cycles'] = weeks.map.with_index {|c, i| i}
      response['weeks'] = mappings

      response
    end

    def query(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .where.not(title: '0').where(indication: 'PREÇO')
                .joins('LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id')
                .joins('LEFT JOIN item_features ON item_features.item_id = answers.item_id')
                .group('answers.title, item_features.sku, item_features.brand_2, itineraries.week_date, itineraries.cycle')
                .order('item_features.sku, itineraries.week_date, itineraries.cycle, total DESC'),
        params
      ).pluck("DISTINCT ON(item_features.sku, itineraries.week_date) item_features.sku, itineraries.cycle, CAST(answers.title AS NUMERIC), item_features.brand_2, COUNT(answers.title) as total, itineraries.week_date").uniq
    end

    def export_query(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .where.not(title: '0').where(indication: 'PREÇO')
                .joins('LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id')
                .joins('LEFT JOIN item_features ON item_features.item_id = answers.item_id')
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end
  end
end
