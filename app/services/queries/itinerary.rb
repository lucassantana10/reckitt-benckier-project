module Queries
  class Itinerary
    def initialize(filters = {}, current_ability = nil, page = nil)
      @current_ability = current_ability
      @filters = filters
    end

    def process
      query_prepare
    end

    def process_map
      map_prepare
    end

    def process_price_assortment
      price_assortment_prepare
    end

    private

    def query_prepare
      array = []
      response = {}
      query(@filters).each do |info|
        array << info
      end
      response['content'] = array
      response['count'] = count(@filters)
      response
    end

    def map_prepare
      array = []
      response = {}
      query_map(@filters).each do |info|
        hash = {}
        hash[:id] = info.id
        hash[:store] = info.store
        hash[:latitude] = info.latitude
        hash[:longitude] = info.longitude
        hash[:state] = info.state_code
        hash[:city] = info.city
        hash[:type] = 'LOJA'

        array << hash
      end

      response['content'] = array
      response['count'] = count(@filters)
      response
    end

    def price_assortment_prepare
      hash = {}
      response = {}
      cycles_list = []

      query_price_assortment(@filters).each do |row|
        hash[row[3]] ||= {}
        hash[row[3]][row[0]] ||= []

        price = "-" if row[1] == "0,00" || row[1] == "0" || row[1] == '' || row[1].nil?
        price = "R$ #{row[1]}" if price.nil?

        hash[row[3]][row[0]] << [row[2], row[4], price]

        cycles_list << row[4] unless row[4].in?(cycles_list)
      end

      response['content'] = hash
      response['cycles'] = cycles_list
      response
    end

    def count(params)
      Queries::TotalStores.new(params, @current_ability).process
    end

    def query(params)
      QueryFilter.filter_relation(
        ::Itinerary.accessible_by(@current_ability)
                   .joins("LEFT JOIN items ON items.itinerary_id = itineraries.id")
                   .joins("LEFT JOIN item_features ON item_features.item_id = items.id"),
        params
      ).limit(@limit).offset(@offset).select('DISTINCT ON(itineraries.pos_tax_code) itineraries.*')
    end

    def query_map(params)
      QueryFilter.filter_relation(
        ::Itinerary.accessible_by(@current_ability)
                   .joins("LEFT JOIN items ON items.itinerary_id = itineraries.id")
                   .joins("LEFT JOIN item_features ON item_features.item_id = items.id"),
        params
      ).select('DISTINCT ON(itineraries.pos_tax_code) itineraries.*')
    end

    def query_price_assortment(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .where(indication: 'PREÇO')
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .order('itineraries.week_date'),
        params
      ).pluck('DISTINCT ON(item_features.sku, itineraries.week_date) item_features.sku, answers.title, item_features.brand_2, item_features.category, itineraries.cycle')
    end
  end
end
