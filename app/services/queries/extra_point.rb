module Queries
  class ExtraPoint
    def initialize(filters = {}, current_ability = nil, page = nil)
      @current_ability = current_ability
      @filters = filters
    end

    def process_by_brand
      query_prepare(:by_brand)
    end

    def process_by_extra_point
      query_prepare(:by_extra_point)
    end

    def process_by_bugspray
      query_prepare_percentage(:by_bugspray)
    end

    def process_by_cleaner
      query_prepare_percentage(:by_cleaner)
    end

    def process_by_bleach
      query_prepare_percentage(:by_bleach)
    end

    def process_export(graph_type)
      case graph_type
      when 'extra_point_bugspray'
        query_export_by_bugspray(@filters)
      when 'extra_point_cleaner'
        query_export_by_cleaner(@filters)
      when 'extra_point_bleach'
        query_export_by_bleach(@filters)
      when 'extra_point_by_brand'
        query_export_by_brand(@filters)
      when 'extra_point_by'
        query_export_by_extra_point(@filters)
      end
    end

    private

    def query_prepare(origin_type)
      hash = {}
      response = {}
      cicles = {}
      brands = []
      response['content'] = []

      case origin_type
      when :by_brand
        query = query_by_brand(@filters)
      when :by_extra_point
        query = query_by_extra_point(@filters)
      end

      query_cicles(@filters).each do |w_d ,cycle|
        cicles[w_d] ||= {}
        cicles[w_d] = cycle
      end

      query.each do |brand, _, cycle, total|
        brands << brand unless brand.in?(brands)
      end

      cicles.each do |_, cycle|
        brands.each do |brand|
          hash[brand] ||= {}
          hash[brand][cycle] ||= 0
        end
      end

      query.each do |brand, _, cycle, total|
        hash[brand][cycle] = total
      end

      hash.each do |key, values|
        response['content'] << {'key' => key , 'values' => values.to_a}
      end
      response
    end

    def query_prepare_percentage(origin_type)
      hash = {}
      response = {}
      cicles = {}
      brands = []
      cicle_total = {}
      absolute = {}

      case origin_type
      when :by_bugspray
        query = query_by_bugspray(@filters)
      when :by_cleaner
        query = query_by_cleaner(@filters)
      when :by_bleach
        query = query_by_bleach(@filters)
      end

      query_cicles(@filters).each do |w_d ,cycle|
        cicles[w_d] ||= {}
        cicles[w_d] = cycle
      end

      query.each do |brand, _, cycle, total|
        brands << brand unless brand.in?(brands)
        cicle_total[cycle] ||= 0
        cicle_total[cycle] += total
      end

      cicles.each do |_, cycle|
        brands.each do |brand|
          hash[brand] ||= {}
          hash[brand][cycle] ||= 0
          absolute[brand] ||= {}
          absolute[brand][cycle] ||= 0
        end
      end

      query.each do |brand, _, cycle, total|
        hash[brand][cycle] = (total.to_f / cicle_total[cycle]).to_f
        absolute[brand][cycle] = total
      end

      response['content'] = []
      hash.each do |key, values|
        response['content'] << {'key' => key , 'values' => values.to_a}
      end
      response['absolute'] = absolute
      response
    end

    def query_by_brand(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'PONTO EXTRA'"),
        params
      ).group('item_features.brand_2, itineraries.week_date, itineraries.cycle')
       .order('itineraries.week_date ASC')
       .pluck('item_features.brand_2, itineraries.week_date, itineraries.cycle, COUNT(item_features.brand_2)')
    end

    def query_by_extra_point(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'PONTO EXTRA' AND item_features.manufacturer = 'RB'"),
        params
      ).group('answers.title, itineraries.week_date, itineraries.cycle')
       .order('itineraries.week_date ASC')
       .pluck('answers.title, itineraries.week_date, itineraries.cycle, COUNT(answers.title)')
    end

    def query_by_bugspray(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'LOCALIZAÇÃO' AND item_features.manufacturer = 'RB' AND item_features.category = 'INSETICIDAS'"),
        params
      ).group('answers.title, itineraries.week_date, itineraries.cycle')
       .order('itineraries.week_date ASC')
       .pluck('answers.title, itineraries.week_date, itineraries.cycle, COUNT(answers.title)')
    end

    def query_by_cleaner(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'LOCALIZAÇÃO' AND item_features.manufacturer = 'RB' AND item_features.category = 'LIMPADORES'"),
        params
      ).group('answers.title, itineraries.week_date, itineraries.cycle')
       .order('itineraries.week_date ASC')
       .pluck('answers.title, itineraries.week_date, itineraries.cycle, COUNT(answers.title)')
    end

    def query_by_bleach(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'LOCALIZAÇÃO' AND item_features.manufacturer = 'RB' AND item_features.category = 'ALVEJANTES'"),
        params
      ).group('answers.title, itineraries.week_date, itineraries.cycle')
       .order('itineraries.week_date ASC')
       .pluck('answers.title, itineraries.week_date, itineraries.cycle, COUNT(answers.title)')
    end

    def query_cicles(params)
      QueryFilter.filter_relation(
        ::Itinerary.accessible_by(@current_ability)
                   .joins("LEFT JOIN items ON items.itinerary_id = itineraries.id")
                   .joins("LEFT JOIN item_features ON item_features.item_id = items.id"),
        params
      ).group(:week_date, :cycle).order(:week_date, :cycle).pluck(:week_date, :cycle).uniq
    end

    def query_export_by_brand(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'PONTO EXTRA'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end

    def query_export_by_extra_point(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'PONTO EXTRA' AND item_features.manufacturer = 'RB'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end

    def query_export_by_bugspray(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'LOCALIZAÇÃO' AND item_features.manufacturer = 'RB' AND item_features.category = 'INSETICIDAS'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end

    def query_export_by_cleaner(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'LOCALIZAÇÃO' AND item_features.manufacturer = 'RB' AND item_features.category = 'LIMPADORES'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end

    def query_export_by_bleach(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'LOCALIZAÇÃO' AND item_features.manufacturer = 'RB' AND item_features.category = 'ALVEJANTES'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end
  end
end
