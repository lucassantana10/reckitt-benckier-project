module Queries
  class ShelfSpace
    def initialize(filters = {}, current_ability = nil, page = nil)
      @current_ability = current_ability
      @filters = filters
    end

    def process_bugspray
      query_prepare(:bugspray)
    end

    def process_cleaner
      query_prepare(:cleaner)
    end

    def process_bleach
      query_prepare(:bleach)
    end

    def process_export(graph_type)

      case graph_type
      when 'shelf_bugspray'
        query_export_bugspray(@filters)
      when 'shelf_cleaner'
        query_export_cleaner(@filters)
      when 'shelf_bleach'
        query_export_bleach(@filters)
      end

    end

    private

    def query_prepare(origin_type)
      response = {}
      shelf = {}
      total_all = {}
      shelf_absolute = {}
      cicles = {}
      brands = []

      case origin_type
      when :bugspray
        query = query_bugspray(@filters)
      when :cleaner
        query = query_cleaner(@filters)
      when :bleach
        query = query_bleach(@filters)
      end

      query_cicles(@filters).each do |w_d ,cycle|
        cicles[w_d] ||= {}
        cicles[w_d] = cycle
      end

      query.each do |brand, _, cycle, total|
        brands << brand unless brand.in?(brands)
      end

      cicles.each do |_, cycle|
        brands.each do |brand|
          shelf[brand+"  "] ||= {}
          shelf[brand+"  "][cycle] = 0
          shelf_absolute[brand+"  "] ||= {}
          shelf_absolute[brand+"  "][cycle] ||= 0
        end
      end

      query.each do |brand, _, cycle, total|
        total_all[cycle] ||= 0
        total_all[cycle] += total.to_f

        shelf_absolute[brand+"  "][cycle] += total.to_f
      end

      query.each do |brand, _, cycle, total|
        shelf[brand+"  "][cycle] = (total.to_f / total_all[cycle] ).to_f
      end

      response['content'] = []
      shelf.each do |key, values|
        response['content'] << { 'key' => key, 'values' => values.to_a}
      end

      response['absolute'] = shelf_absolute.as_json.merge(shelf_absolute)
      response
    end

    def query_bugspray(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'MEDIDA' AND item_features.category = 'INSETICIDAS'"),
        params
      ).group('item_features.brand_2, itineraries.week_date, itineraries.cycle')
       .pluck('item_features.brand_2, itineraries.week_date, itineraries.cycle, SUM(CAST(answers.title AS FLOAT))')
    end

    def query_cleaner(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'MEDIDA' AND item_features.category = 'LIMPADORES'"),
        params
      ).group('item_features.brand_2, itineraries.week_date, itineraries.cycle')
       .pluck('item_features.brand_2, itineraries.week_date, itineraries.cycle, SUM(CAST(answers.title AS FLOAT))')
    end

    def query_bleach(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'MEDIDA' AND item_features.category = 'ALVEJANTES'"),
        params
      ).group('item_features.brand_2, itineraries.week_date, itineraries.cycle')
       .pluck('item_features.brand_2, itineraries.week_date, itineraries.cycle, SUM(CAST(answers.title AS FLOAT))')
    end

    def query_cicles(params)
      QueryFilter.filter_relation(
        ::Itinerary.accessible_by(@current_ability)
                   .joins("LEFT JOIN items ON items.itinerary_id = itineraries.id")
                   .joins("LEFT JOIN item_features ON item_features.item_id = items.id"),
        params
      ).group(:week_date, :cycle).order(:week_date, :cycle).pluck(:week_date, :cycle).uniq
    end

    def query_export_bleach(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'MEDIDA' AND item_features.category = 'ALVEJANTES'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end

    def query_export_cleaner(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'MEDIDA' AND item_features.category = 'LIMPADORES'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end

    def query_export_bugspray(params)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'MEDIDA' AND item_features.category = 'INSETICIDAS'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end

  end
end
