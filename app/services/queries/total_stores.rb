module Queries
  class TotalStores
    def initialize(filters = {}, current_ability = nil)
      @current_ability = current_ability
      @filters = filters
    end

    def process
      process_total_stores
    end

    private

    def process_total_stores
      array = []
      response = {}

      response['total_stores_filter'] = total_stores_prepare(@filters)
      response['total_stores'] = 50
      response
    end

    def total_stores_prepare(params)
      QueryFilter.filter_relation(
        ::Itinerary.accessible_by(@current_ability)
                   .joins("LEFT JOIN items ON items.itinerary_id = itineraries.id")
                   .joins("LEFT JOIN item_features ON item_features.item_id = items.id")
                   .select('DISTINCT pos_tax_code')
                   .where(week_date: ::Itinerary.last.week_date),
        params
      ).uniq.count
    end
  end
end
