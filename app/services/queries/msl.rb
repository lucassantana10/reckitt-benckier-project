module Queries
  class Msl
    def initialize(filters = {}, current_ability = nil, page = nil)
      @current_ability = current_ability
      @filters = filters
    end

    def process_segment
      query_prepare(:segment)
    end

    def process_retail
      query_prepare(:retail)
    end

    def process_export(graph_type)
      case graph_type
      when 'msl_segment'
        query_export(@filters, :segment)
      when 'msl_retail'
        query_export_retail(@filters, :retail)
      end
    end

    private

    def query_prepare(origin_type)
      hash = {}
      response = {}
      msls = {}

      query(@filters, origin_type).each do |row|
        hash[row[0]] ||= {}
        hash[row[0]][row[3]] ||= {}
        hash[row[0]][row[3]][row[2]] ||= []
        hash[row[0]][row[3]][row[2]] << row[1] unless row[1].in?(hash[row[0]][row[3]][row[2]])
      end

      query_msl(origin_type).each do |row|
        msls[row[0]] ||= {}
        msls[row[0]][row[1]] ||= []
        msls[row[0]][row[1]] << row[2]
      end

      # p msls

      hash.each do |key, vals|
        vals.each do |cycle, stores|
          stores.each do |store, products|
            total_found = 0
            products.each { |e| total_found += 1 if e.in?(msls[key][cycle]) }
            hash[key][cycle][store] = (total_found.to_f / msls[key][cycle].count)
          end
        end
      end

      hash.each do |key, vals|
        vals.each do |cycle, stores|
          store_count = stores.count
          score_sum = 0
          stores.each { |_, score| score_sum += score }
          hash[key][cycle] = (score_sum / store_count.to_f)
        end
      end

      response['content'] = []
      hash.each do |key, values|
        response['content'] << {'key' => key + '   ', 'values' => values.to_a}
      end

      response
    end

    def query(params, origin_type)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .where("answers.indication = 'PREÇO' AND item_features.manufacturer = 'RB'"),
        params
      ).pluck("itineraries.#{origin_type}, item_features.sku, itineraries.pos_tax_code, itineraries.cycle")
    end

    def query_msl(param)
      ::Msl.group("#{param}, cycle, product").pluck("#{param}, cycle, product")
    end

    def query_export(params, origin_type)
      QueryFilter.filter_relation(
        ::Answer.accessible_by(@current_ability)
                .joins("LEFT JOIN itineraries ON itineraries.id = answers.itinerary_id")
                .joins("LEFT JOIN item_features ON item_features.item_id = answers.item_id")
                .joins("LEFT JOIN questions ON questions.id = answers.question_id")
                .joins("LEFT JOIN forms ON forms.id = questions.form_id")
                .where("answers.indication = 'PREÇO'"),
        params
      ).pluck('itineraries.cycle, itineraries.execution_date_time, itineraries.store, itineraries.segment, itineraries.pos_tax_code, itineraries.economic_group, itineraries.retail, itineraries.area_manager, itineraries.postal_code, itineraries.address, itineraries.neighborhood, itineraries.city, itineraries.state_code, itineraries.region, forms.title, questions.title, answers.title, answers.item_id, item_features.sku, item_features.brand, item_features.manufacturer, item_features.category, item_features.brand_2')
    end
  end
end
