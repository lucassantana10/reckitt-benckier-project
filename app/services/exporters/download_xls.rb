module Exporters
  require 'csv'
  class DownloadXls
    def initialize(filters = {}, current_ability = nil, origin_type, origin_graph)
      @filters = filters
      @current_ability = current_ability
      @origin_type = origin_type
      @origin_graph = origin_graph
    end

    def process
      worksheet(col_sep: ';')
    end

    def query
      case @origin_type
      when 'shelf_space'
        Queries::ShelfSpace.new(@filters, @current_ability).process_export(@origin_graph)
      when 'extra_point'
        Queries::ExtraPoint.new(@filters, @current_ability).process_export(@origin_graph)
      when 'price'
        Queries::Price.new(@filters, @current_ability).process_export(@origin_graph)
      when 'msl'
        Queries::Msl.new(@filters, @current_ability).process_export(@origin_graph)
      end
    end

    private

    def worksheet(options = {})
      (CSV.generate(options) do |csv|
        csv << column_names
        query.each do |record|
          csv << record
        end
      end).encode('WINDOWS-1252', undef: :replace, replace: '')
    end

    def column_names
      ['CYCLE', 'EXECUTION_DATE', 'STORE', 'CLASSIFICATION', 'DOCUMENT', 'CHANNEL', 'RETAIL', 'FLAG', 'ZIP_CODE', 'ADDRESS', 'NEIGHBORHOOD', 'CITY', 'STATE_CODE', 'REGION', 'FORM_NAME', 'QUESTION', 'ANSWER', 'ITEM ID', 'PRODUCT', 'BRAND', 'FABRICANTE', 'SEGMENTO', 'BRAND 2']
    end
  end
end
