module Importers
  class ItineraryImporter
    def initialize(visit_id)
      rows = itinerary = Onetrade.where(visit_id: visit_id)
      @items = rows.group_by(&:item_id)
      @itinerary = itinerary.uniq[0]
      @issues ||= {}
    end

    def process
      return false if @itinerary.blank?
      return false if @items.blank?
      p "Doing #{@itinerary.visit_id}"

      process_itinerary
    end

    private

    def process_itinerary
      success = true
      ActiveRecord::Base.transaction do
        itinerary = create_itinerary(@itinerary)

        if itinerary.persisted?
          @items.each do |key, values|
            success &= process_items(key, itinerary, values)
          end
        else
          @issues['itinerary'] =
            itinerary.errors.full_messages.join("\n")
        end
        fail ActiveRecord::Rollback if !@issues.empty? || !success
      end
    end

    def create_itinerary(itinerary)
      object = {}
      object[:pos_tax_code]        = itinerary.code.gsub(/[^0-9]/, '').to_i
      object[:store]               = itinerary.store
      object[:visit_id]            = itinerary.visit_id
      object[:execution_date_time] = itinerary.execution_date_time
      object[:retail]              = itinerary.retail
      object[:shopping]            = itinerary.shopping
      object[:postal_code]         = itinerary.postal_code
      object[:address]             = itinerary.address
      object[:neighborhood]        = itinerary.neighborhood
      object[:city]                = itinerary.city
      object[:state_code]          = itinerary.state_code
      object[:region]              = itinerary.region
      object[:longitude]           = itinerary.longitude
      object[:latitude]            = itinerary.latitude
      object[:week_date]           = itinerary.week_date
      object[:cycle]               = itinerary.cycle
      object[:segment]             = itinerary.classification
      object[:economic_group]      = itinerary.channel
      object[:area_manager]        = itinerary.pos_flag

      Itinerary.create(object)
    end

    def process_picture(itinerary, url, indication, title, category, item)
      return false if itinerary.nil?
      return false if url.nil?
      return false if indication.nil?

      object = {}
      object[:itinerary_id] = itinerary.id
      object[:item_id] = item.id if item.present?
      object[:url] = url
      object[:indication] = indication
      object[:category] = category
      object[:title] = title
      Picture.create(object)
    end

    def process_answer(itinerary, question_processed, answer, product,
                       indication, item, answered_at)
      return false if itinerary.nil?
      return false if question_processed.nil?
      return false if answer.nil?
      return false if item.nil?

      object = {}
      object[:question_id] = question_processed.id
      object[:itinerary_id] = itinerary.id
      object[:title] = answer
      object[:product] = product
      object[:indication] = indication
      object[:item_id] = item.id
      object[:answered_at] = answered_at

      Answer.create(object)
    end

    def process_form(form)
      return false if form.nil?

      Form.find_or_create_by(title: form)
    end

    def process_question(question, form)
      return false if question.nil?
      return false if form.nil?

      Question.find_or_create_by(title: question, form_id: form.id)
    end

    def process_items(key, itinerary, values)
      return false if itinerary.nil?
      return false if values.blank?

      item = Item.find_or_create_by(itinerary_id: itinerary.id, key: key) unless key.nil?

      values.each do |value|
        next if value.form_name == 'MEDIÇÃO DA CATEGORIA'
        next if value.indication.nil? || value.indication == ''
        if value.indication == 'FOTO'
          process_picture(itinerary, value.answer, value.indication, value.question, value.category, item)
        else
          question = value.question
          indication = value.indication
          product = value.product
          answer_text = value.answer
          form = value.form_name
          form_processed = process_form(form)
          question_processed = process_question(question, form_processed)
          answered_at = value.answered_at

          process_answer(itinerary, question_processed, answer_text, product,
                         indication, item, answered_at) if question_processed != false

          process_item_features(item, value)
        end
      end
    end

    def process_item_features(item, value)
      return false if item.nil?
      return false if value.nil?
      return false if ItemFeature.find_by(item_id: item.id)

      object = {}
      object[:item_id] = item.id
      object[:category] = value.segmento
      object[:subcategory] = value.product_category
      object[:manufacturer] = value.fabricante
      object[:brand] = value.brand
      object[:brand_2] = value.brand_2
      object[:sku] = value.product

      ItemFeature.create(object)
    end

  end
end
