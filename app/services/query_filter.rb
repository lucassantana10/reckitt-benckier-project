class QueryFilter
  def self.filter_relation(relation, params)
    sql = relation
    params.each do |table, values|
      values.each do |field, values2|
        values2.each do |type, value|
          case type
          when 'kpi_less_than_70_percent'
            if value.kind_of?(Array)
              itinerary_ids = []
              value.map do |kpi|
                itinerary_ids << Score.joins(:kpi).where(kpis: { name: kpi })
                                      .where('scores.points <  (SELECT (total_points*0.7) FROM kpis WHERE kpis.id = scores.kpi_id)')
                                      .pluck(:itinerary_id)
              end
            end
            itinerary_ids = itinerary_ids.flatten.select { |e|
              itinerary_ids.flatten.count(e) >= value.count
            }.uniq unless value.count == 1
            if !itinerary_ids.blank?
              condition = "itineraries.id IN('#{itinerary_ids.join("','")}')"
            else
              condition = '1=2'
            end
          when 'kpi_less_than_50_percent'
            if value.kind_of?(Array)
              itinerary_ids = []
              value.map do |kpi|
                itinerary_ids << Score.joins(:kpi).where(kpis: { name: kpi })
                                      .where('scores.points <  (SELECT (total_points/2) FROM kpis WHERE kpis.id = scores.kpi_id)')
                                      .pluck(:itinerary_id)
              end
            end
            itinerary_ids = itinerary_ids.flatten.select { |e|
              itinerary_ids.flatten.count(e) >= value.count
            }.uniq unless value.count == 1
            if !itinerary_ids.blank?
              condition = "itineraries.id IN('#{itinerary_ids.join("','")}')"
            else
              condition = '1=2'
            end
          when 'array'
            condition = "#{table}.#{field} IN('#{value.values.flatten.join("','")}')"
          when 'start_date'
            value = value.to_datetime.beginning_of_day
            condition = "#{table}.#{field} >=  ?"
          when 'end_date'
            value = value.to_datetime.end_of_day
            condition = "#{table}.#{field} <=  ?"
          when 'date'
            value = value.to_date
            condition = "DATE(#{table}.#{field}) = ?"
          when 'month'
            value = value.to_date
            condition = "DATE_TRUNC('month', #{table}.#{field}) = ?"
          when 'is_null'
            condition = "#{table}.#{field} IS NULL"
          when 'is_not_null'
            condition = "#{table}.#{field} IS NOT NULL"
          when 'custom'
            condition = ''
          else
            if value.kind_of?(Array)
              condition = "#{table}.#{field} IN('#{value.join("','")}')"
            else
              condition = "#{table}.#{field} = ?"
            end
          end
          if ['is_null', 'is_not_null', 'array'].include?(type)
            sql = sql.where(condition)
          else
            sql = sql.where(condition, value) unless value.blank?
          end
        end
      end
    end
    sql
  end
end
