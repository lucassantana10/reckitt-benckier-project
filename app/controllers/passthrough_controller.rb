class PassthroughController < ApplicationController
  def index

    path = case current_user.role.name
           when 'Administrator', 'Coordenador'
             maps_path
           else
             itinerary = current_user.itineraries.first
             itinerary_path(itinerary) unless itinerary.nil?
           end

    redirect_to path unless path.nil?
  end
end
