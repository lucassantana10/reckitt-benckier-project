module Api
  module V1
    class BaseController < ApplicationController
      before_action :authenticate
      protect_from_forgery

      private

      def authenticate
        :authenticate_user!
      end
    end
  end
end
