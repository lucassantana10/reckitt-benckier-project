module Api
  module V1
    class FiltersController < Api::V1::BaseController
      load_and_authorize_resource :itinerary, :parent => false
      respond_to :json

      def index
        @data = case params[:origin_type]
                when 'default'
                  ::Filters::Default.new(filter_params, current_ability).process_default
                when 'store'
                  ::Filters::Default.new(filter_params, current_ability).process_store
                end
      end

      private

      def filter_params
        params[:filter].blank? ? default_params : default_params.merge!(params[:filter])
      end

      def default_params
        {}
      end
    end
  end
end
