module Api
  module V1
    class ExportsController < Api::V1::BaseController
      respond_to :json

      def index
        @data = Exporters::DownloadXls.new(filter_params, current_ability, params[:origin_type], params[:origin_graph]).process
        
        respond_to do |format|
          format.csv { send_data @data }
        end
      end

      private

      def filter_params
        params[:filter].blank? ? default_params : default_params.merge!(params[:filter])
      end

      def default_params
        {}
      end
    end
  end
end
