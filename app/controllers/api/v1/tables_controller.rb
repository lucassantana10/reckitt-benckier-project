module Api
  module V1
    class TablesController < Api::V1::BaseController
      load_and_authorize_resource :itinerary, :parent => false
      respond_to :json

      def index
        @data = case params[:origin_type]
                when 'total_stores'
                  Queries::TotalStores.new(last_week_filter_params, current_ability).process
                when 'itineraries'
                  Queries::Itinerary.new(filter_params, current_ability, page_params).process
                when 'pictures'
                  Queries::Picture.new(filter_params, current_ability, page_params).process
                when 'map'
                  Queries::Itinerary.new(filter_params, current_ability).process_map
                when 'shelf_space_bugspray'
                  Queries::ShelfSpace.new(filter_params, current_ability).process_bugspray
                when 'shelf_space_cleaner'
                  Queries::ShelfSpace.new(filter_params, current_ability).process_cleaner
                when 'shelf_space_bleach'
                  Queries::ShelfSpace.new(filter_params, current_ability).process_bleach
                when 'extra_point_by_brand'
                  Queries::ExtraPoint.new(filter_params, current_ability).process_by_brand
                when 'extra_point_by_extra_point'
                  Queries::ExtraPoint.new(filter_params, current_ability).process_by_extra_point
                when 'extra_point_by_bugspray'
                  Queries::ExtraPoint.new(filter_params, current_ability).process_by_bugspray
                when 'extra_point_by_cleaner'
                  Queries::ExtraPoint.new(filter_params, current_ability).process_by_cleaner
                when 'extra_point_by_bleach'
                  Queries::ExtraPoint.new(filter_params, current_ability).process_by_bleach
                when 'price'
                  Queries::Price.new(price_filter_params, current_ability).process
                when 'msl_segment'
                  Queries::Msl.new(filter_params, current_ability).process_segment
                when 'msl_retail'
                  Queries::Msl.new(retail_filter_params, current_ability).process_retail
                when 'price_assortment'
                  Queries::Itinerary.new(filter_params, current_ability).process_price_assortment
                end
      end

      private

      def filter_params
        params[:filter].blank? ? default_params : default_params.merge!(params[:filter])
      end

      def page_params
        params[:page].blank? ? 0 : params[:page]
      end

      def price_filter_params
        if params[:filter].present? && params[:filter][:item_features].present? && params[:filter][:item_features][:sku].present?
          price_default_params.merge!(params[:filter])
        else
          price_default_params
        end
      end

      def retail_filter_params
        if params[:filter].present? && params[:filter][:itineraries].present? && params[:filter][:itineraries][:retail].present?
          retail_default_params.merge!(params[:filter])
        else
          retail_default_params
        end
      end

      def last_week_filter_params
        params[:filter].blank? ? last_week_default_params : last_week_default_params.merge!(params[:filter])
      end

      def default_params
        {}
      end

      def price_default_params
        {
          'item_features' =>
          {
            'sku' => { array: ['VEJA MUSO', 'VEJA X-14 BANHEIRO SPRAY 500 M',
                               'OE SBP MULTI INSET', 'VANISH SUPER BARRA TIRA MANCHA']
            }
          }
        }
      end

      def last_week_default_params
        {
          'itineraries' =>
          {
            'week_date' => { array: [::Itinerary.last.week_date]
            }
          }
        }
      end

      def retail_default_params
        {
          'itineraries' =>
          {
            'retail' => { array: ["GIGA ATACADO", "MERCADAO ATACADISTA",
                               "SETA ATACADISTA", "5M COMERCIO ATACADISTA E VAREJISTA DE ALIMENTOS"]
            }
          }
        }
      end
    end
  end
end
