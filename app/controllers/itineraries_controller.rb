class ItinerariesController < ApplicationController
  load_and_authorize_resource
  before_action :set_itinerary, only: [:show]

  def index
  end

  def show
    @itinerary_gallery = @itinerary.gallery
    @itinerary_occurrence = @itinerary.occurrence
  end

  private

  def set_itinerary
    @itinerary = Itinerary.find(params[:id])
  end
end
