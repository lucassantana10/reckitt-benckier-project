class ItemFeature < ActiveRecord::Base
  belongs_to :item
  has_one :itinerary, :through => :item
end
