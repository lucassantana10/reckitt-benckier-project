class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :itinerary

  validates_presence_of :question_id
  validates_presence_of :title
  validates_presence_of :itinerary_id
end
