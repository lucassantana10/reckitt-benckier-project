class ItineraryMeta < ActiveRecord::Base
  self.table_name = "itinerary_meta"
  belongs_to :itinerary
end
