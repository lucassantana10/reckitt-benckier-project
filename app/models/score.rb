class Score < ActiveRecord::Base
  belongs_to :itinerary
  belongs_to :kpi

  validates_presence_of :itinerary_id
  validates_presence_of :kpi_id
end
