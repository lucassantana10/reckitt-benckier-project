class Rule < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :question

  validates_presence_of :question_id
  validates_presence_of :points
end
