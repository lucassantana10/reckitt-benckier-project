class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :role

  validates_presence_of :name, :role_id

  has_and_belongs_to_many :itineraries

  default_scope { order('name ASC') }

  scope :coordinators, lambda {
    where('role_id = ?', 3)
  }


  def role?(role_sym)
    role.name.downcase.gsub(/( )/, '_').to_sym == role_sym
  end


  def name
    if self[:name] == "Administrador"
      'TRACKINGTRADE'
    else
      self[:name]
    end
  end

end
