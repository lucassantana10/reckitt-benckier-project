class Question < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :form
  has_many :answers

  validates_presence_of :title
end
