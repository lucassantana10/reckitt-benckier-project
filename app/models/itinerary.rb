class Itinerary < ActiveRecord::Base
  acts_as_paranoid

  has_and_belongs_to_many :users
  has_many :pictures
  has_many :items
  has_many :item_features, through: :items
  has_many :answers
  has_many :scores
  has_many :points

  validates_presence_of :pos_tax_code
  validates_presence_of :visit_id
  validates_uniqueness_of :visit_id

  def gallery
    gallery = {}

    front = self.pictures.where(category: 'FACHADA').first
    gallery['front'] = front.nil? ? false : front.url

    extra_points = self.pictures
                       .joins("LEFT JOIN item_features ON item_features.item_id = pictures.item_id")
                       .joins("LEFT JOIN itineraries ON itineraries.id = pictures.itinerary_id")
                       .where("pictures.category = 'PONTO EXTRA'")
                       .pluck("DISTINCT ON (pictures.id) pictures.url, item_features.sku, itineraries.cycle")
    gallery['extra_points'] = extra_points.nil? ? false : extra_points.to_a

    gallery['gallery'] = self.pictures
                             .joins("LEFT JOIN item_features ON item_features.item_id = pictures.item_id")
                             .joins("LEFT JOIN itineraries ON itineraries.id = pictures.itinerary_id")
                             .where("pictures.category IN ('CONFINADA','PONTO PRINCIPAL','EXPOSIÇÃO')")
                             .pluck("DISTINCT ON (pictures.id) pictures.url, pictures.title, item_features.sku, itineraries.cycle")

    gallery
  end

  def msl
    hash = {}
    items = {}
    response = {}
    products = Msl.where(retail: retail).pluck(:cycle, :product)
    products.map do |cycle, product|
      hash[cycle] ||= []
      hash[cycle] << product
    end

    hash.each do |key, value|
      items[key] = ItemFeature.where(sku: value).where(manufacturer: 'RB').joins(:item)
                              .joins(:itinerary)
                              .where(itineraries: { cycle: key, store: store } )
                              .distinct.count(:sku)
    end

    items.each do |key, value|
      response[key] ||= 0
      response[key] = (items[key].to_f / hash[key].count.to_f).to_f
    end

    response.sort
  end

  def occurrence
    occurrence = self.answers.where(indication: 'OCORRÊNCIA').pluck(:title).first
    if occurrence == "OUTRA OCORRÊNCIA"
      return self.answers.where(indication: 'DESCRIÇÃO').pluck(:title).first
    else
      return occurrence
    end
  end
end
