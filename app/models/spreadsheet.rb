class Spreadsheet < ActiveRecord::Base
  has_attached_file :file
  validates_attachment_presence :file
  validates_attachment_content_type(
    :file,
    content_type: [
      'application/vnd.ms-excel',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'text/csv'
    ]
  )
  SOURCE_TYPES = {
    (RULES = 'rules') => 'Rules',
    (HIERARCHY = 'hierarchy') => 'Hierarchy'
  }
  scope :rules, -> { where(source_type: RULES) }
  scope :hierarchy, -> { where(source_type: HIERARCHY) }
end
