class Item < ActiveRecord::Base
  has_many :pictures
  belongs_to :itinerary
  has_many :answers
  has_many :item_features
end
