class Job < ActiveRecord::Base


	def self.get_link job_id

		job = Job.find_by(job_id: job_id)
		
		if !job.url.nil?
			return job.url
		else
			job.url = ""
			return 	job.url
		end	
				
	end	
end
