class Kpi < ActiveRecord::Base
  acts_as_paranoid

  validates_presence_of :name
  validates_presence_of :total_points
  has_many :questions
  has_many :superquestions, -> { where parent_id: nil }, class_name: 'Question'

  def self.total_percentage_count(value)
    total_points = Kpi.pluck('SUM(total_points)').first
    value = ((value.to_f / total_points) * 100).round(1)
    value.to_s + '%'
  end

  def total_points_by_itinerary(itinerary)
    Point.where(itinerary_id: itinerary.id,
                question_id: questions.map(&:id)
               ).sum(:value)
  end

  def gallery_by_itinerary_id(itinerary_id)
    Picture.where(indicator: name.parameterize(' ').upcase,
                  imageable_id: itinerary_id,
                  imageable_type: 'Itinerary'
                 )
  end

  def self.percentage_kpi(kpi, value)
    total_points = Kpi.where(name: kpi).pluck(:total_points).first
    value = ((value.to_f / total_points) * 100).round(1)
    value.to_s + '%'
  end

end
