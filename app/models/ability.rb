class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user

    managed_itineraries = []
    user.itineraries.each { |i| managed_itineraries << i.id }

    if user.role? :administrator
      can :manage, :all
    end

    if user.role? :coordenador
      can :manage, :ranking
      can :manage, :map
      can :manage, :picture
      can :manage, Itinerary, id: managed_itineraries
      can :manage, Picture, imageable_id: managed_itineraries, imageable_type: 'Itinerary'
    end

    if user.role? :unidade
      can :read, Itinerary, id: managed_itineraries
      cannot :index, Itinerary
    end

  end
end
