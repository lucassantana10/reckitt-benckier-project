class Picture < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :itinerary
  belongs_to :item
end
