module ItineraryHelper
  def perfect_kpi?(points)
    if points < 70
      'store-icon-line-red'
    else
      'store-icon-line'
    end
  end
end
