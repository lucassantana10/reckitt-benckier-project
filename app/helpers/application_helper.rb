module ApplicationHelper

  def current_page_class(link_path)
    class_name = current_page?(link_path) ? 'active' : ''
  end

  def error_messages_for resource
    return "" if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t("errors.messages.not_saved",
    count: resource.errors.count,
    resource: resource.class.model_name.human.downcase)

    html = <<-HTML
    <div class="alert alert-dismissable alert-danger">
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end

  def sum_all_kpis(store)
    sum = store.presentation_kpi_points + store.exposition_kpi_points + store.merchandising_kpi_points + store.relationship_kpi_points + store.rupture_kpi_points + store.share_kpi_points
  end

  def max_record_updated_at
    Record.last.updated_at
  end

end
