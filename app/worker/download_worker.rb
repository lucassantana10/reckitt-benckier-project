class DownloadWorker
  include Sidekiq::Worker
  
  def perform(name,job_id,filter_params)

    puts "Starting the job!"

    puts "Creating temporary file..."

    case name
    when 'coordinator'
      report = Query::Ranking.export_coordinators_xls(query: filter_params)
    
    when 'supervisor'
      report = Query::Ranking.export_supervisors_xls(query: filter_params)         
      
    else  
      
    end  

      Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(name: name) do |sheet|

        sheet.add_row Query::Ranking.header_xls

        report.each do |record|
          sheet.add_row record
        end
      end
      #send_data p.to_stream.read, type: "application/xlsx", filename: "xls_#{report_param}.xlsx"
        xls_data = p.to_stream.read  
        uui = SecureRandom.uuid
        obj = S3_BUCKET.objects.create("tmp/#{uui}.xlsx", xls_data)
        puts "Creating s3 link..." + obj.inspect

        @data_url = obj.url_for(:get, { :expires => 525600.minutes.from_now, :secure => false }).to_s
        
        job = Job.find_by(id: job_id)
        job.url = @data_url
        job.processing = false
        job.save
        
        puts "Get link to s3 " + @data_url

    end



   Sidekiq::Queue.new("infinity").clear
   Sidekiq::RetrySet.new.clear
   Sidekiq::ScheduledSet.new.clear

    puts "Finishing the job!"
  end

end 