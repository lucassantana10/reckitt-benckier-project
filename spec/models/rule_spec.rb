require 'rails_helper'

RSpec.describe Rule, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of(:question_id) }
    it { is_expected.to validate_presence_of(:points) }
  end
end
