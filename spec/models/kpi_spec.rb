require 'rails_helper'

RSpec.describe Kpi, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:total_points) }
  end
end
