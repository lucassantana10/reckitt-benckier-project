require 'rails_helper'

RSpec.describe Answer, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of(:question_id) }
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:itinerary_id) }
  end
end
