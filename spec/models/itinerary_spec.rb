require 'rails_helper'

RSpec.describe Itinerary, type: :model do
  context 'Validations' do
    [:pos_tax_code, :itinerary_id].each do |attr|
      it { is_expected.to validate_presence_of(attr) }
    end
    [:itinerary_id].each do |attr|
      it { is_expected.to validate_uniqueness_of(attr) }
    end
  end
end
