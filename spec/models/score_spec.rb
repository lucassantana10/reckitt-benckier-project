require 'rails_helper'

RSpec.describe Score, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of(:itinerary_id) }
    it { is_expected.to validate_presence_of(:kpi_id) }
  end
end
