require 'rails_helper'

RSpec.describe Question, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of(:kpi_id) }
    it { is_expected.to validate_presence_of(:title) }
  end
end
