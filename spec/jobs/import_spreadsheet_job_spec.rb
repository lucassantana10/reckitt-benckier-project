require 'rails_helper'

RSpec.describe ImportSpreadsheetJob, type: :job do
  describe 'Rules Spreadsheet' do
    let(:spreadsheet1) { FactoryGirl.create(:spreadsheet, :rules) }
    let(:spreadsheet2) { FactoryGirl.create(:spreadsheet, :hierarchy) }

    subject do
      ImportSpreadsheetJob
        .new(spreadsheet1.id)
        .perform_now
    end

    it { expect { subject }.to change { Question.count }.by(118) }
  end
end
