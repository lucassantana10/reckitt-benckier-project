require 'rails_helper'

RSpec.describe Importers::HierarchySpreadsheetImporter, type: :model do
  let!(:itinerary
  ) { FactoryGirl.create(:itinerary, pos_tax_code: '25760877001426') }
  let!(:user) { FactoryGirl.create(:user) }
  let(:spreadsheet) { FactoryGirl.create(:spreadsheet, :hierarchy) }
  let(:service) { Importers::HierarchySpreadsheetImporter.new(spreadsheet) }

  subject do
    service.process
  end

  it do
    subject
    spreadsheet.reload
    expect(spreadsheet.success).to be_truthy
    expect(spreadsheet.processed_at).to be_present
    expect(spreadsheet.issues).to be_blank
  end

  context 'Empty Spreadsheet' do
    let(:spreadsheet) { FactoryGirl.create(:spreadsheet, :empty) }
    it { expect(subject).to be_falsey }
    it do
      subject

      expect(spreadsheet.reload.issues['spreadsheet'])
        .to eq('Planilha Inválida.')
    end
  end

  context 'Wrong Minifloor Pos Tax Code' do
    let!(:itinerary) { FactoryGirl.create(:itinerary, pos_tax_code: '00000000000001')}

    it { expect(subject).to be_falsey }
    it do
      subject

      expect(spreadsheet.reload.issues['line 2'])
        .to eq('Loja com cnpj 25760877001426 não foi encontrada.')
    end
  end
  context 'Wrong User' do
    let!(:user) { FactoryGirl.create(:user, name: 'FULANO DE TAL') }

    it { expect(subject).to be_falsey }
    it do
      subject

      expect(spreadsheet.reload.issues['line 2'])
        .to eq('Usuário com o nome JOSE MARQUES NETO não foi encontrado.')
    end
  end
end
