require 'rails_helper'

RSpec.describe Importers::RulesSpreadsheetImporter, type: :model do
  let(:spreadsheet) { FactoryGirl.create(:spreadsheet, :rules) }
  let(:service) { Importers::RulesSpreadsheetImporter.new(spreadsheet) }

  subject do
    service.process
  end

  it do
    subject
    spreadsheet.reload
    expect(spreadsheet.success).to be_truthy
    expect(spreadsheet.processed_at).to be_present
    expect(spreadsheet.issues).to be_blank
  end

  context 'Empty Spreadsheet' do
    let(:spreadsheet) { FactoryGirl.create(:spreadsheet, :empty) }
    it { expect(subject).to be_falsey }
    it do
      subject

      expect(spreadsheet.reload.issues['spreadsheet'])
        .to eq('Planilha Inválida.')
    end
  end
end
