FactoryGirl.define do
  factory :spreadsheet do
    issues {}
    processed_at nil
    success nil

    [
      :rules, :empty, :hierarchy
    ].each do |source|
      trait source do
        file do
          File.open(
            "#{Rails.root}/spec/support/fixtures/spreadsheets/#{source}.xlsx")
        end
        source_type source
      end
    end
  end
end
