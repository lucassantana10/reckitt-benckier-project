FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@example.com" }
    name 'JOSE MARQUES NETO'
    role_id 5
    password 'password'
    password_confirmation 'password'
  end
end
