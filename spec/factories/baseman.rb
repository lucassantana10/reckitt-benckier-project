FactoryGirl.define do
  factory :baseman do
    itinerary_id 1
    sequence(:pos_tax_code) { |i| "#{i}" }
  end
end
