require 'rest_client'
require 'uri'

namespace :postracker do

  desc "TODO"
  task sync_pos: :environment do

    @current_user = 'fulano@fulano.com.br'

    puts 'Seting up Stores...'
    resource = RestClient::Resource.new("#{Rails.application.config.postracker[:base_url]}/api/v1/external_setup.json", headers: { charset: "utf-8", authorization: "#{ActionController::HttpAuthentication::Token.encode_credentials(Rails.application.config.postracker[:token])}" })
    begin
      parsed_response = []
      resource.get(params: { request_user: @current_user }) do |response, request, result, &block|
        response.return!(request, result, &block) if response.code != 200

        # parsed_response = ActiveSupport::JSON.decode(response).each do |record|
        #   Store.create(record)
        # end
      end
    end while parsed_response.size > 0

    # puts 'Gathering store data...'
    # resource = RestClient::Resource.new("#{Rails.application.config.postracker[:base_url]}/api/v1/external_data.json", headers: { charset: "utf-8", authorization: "#{ActionController::HttpAuthentication::Token.encode_credentials(Rails.application.config.postracker[:token])}" })
    # # Store.each do |store|
    # ['91451', '5936', '7556', '8218', '15590'].each do |store|
    #   parsed_response = []
    #   resource.get(params: { request_user: @current_user, store: store }) do |response, request, result, &block|
    #     p response
    #     abort
    #     response.return!(request, result, &block) if response.code != 200
    #
    #     # parsed_response = ActiveSupport::JSON.decode(response).each do |record|
    #     #   Postracker.create(record)
    #     # end
    #   end
    # end

    puts 'Gathering ranking data...'
    resource = RestClient::Resource.new("#{Rails.application.config.postracker[:base_url]}/api/v1/external_ranking_data.json", headers: { charset: "utf-8", authorization: "#{ActionController::HttpAuthentication::Token.encode_credentials(Rails.application.config.postracker[:token])}" })
    # Store.each do |store|
      parsed_response = []
      resource.get(params: { request_user: @current_user}) do |response, request, result, &block|
        p ActiveSupport::JSON.decode(response)
        abort
        response.return!(request, result, &block) if response.code != 200
      end
  end
end
