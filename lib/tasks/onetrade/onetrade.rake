require 'benchmark'
require 'rest_client'
require 'uri'

namespace :onetrade do

  desc "TODO"
  task sync: :environment do

    bm = Benchmark.measure do

      campaign_id = Rails.application.config.onetrade[:campaign_id]
      resource = RestClient::Resource.new("#{Rails.application.config.onetrade[:base_url]}/api/v1/external.json", headers: { charset: "utf-8", authorization: "#{ActionController::HttpAuthentication::Token.encode_credentials(Rails.application.config.onetrade[:token])}" })

      begin
        max_ot_id = Onetrade.maximum(:ot_id)

        puts "Quering from OT_ID: #{max_ot_id}"

        parsed_response = []
        resource.get(params: { ot_id: max_ot_id, campaign_id: campaign_id }) do |response, request, result, &block|

          response.return!(request, result, &block) if response.code != 200

          parsed_response = ActiveSupport::JSON.decode(response).each do |record|
            Onetrade.create(record)
          end
        end
      end while parsed_response.size > 0
    end

    puts bm.inspect

    generate

    populate_msl

    puts 'Finishing!'
  end

  task generate: :environment do
    generate
  end
  def generate
    puts 'Populating tables...'
    ImportDataOnetradeJob.perform_now
  end

  task populate_msl: :environment do
    populate_msl
  end
  def populate_msl
    puts 'Populating MSL...'

    Msl.delete_all
    file = JSON.parse(File.read(Rails.root + 'lib/assets/msl.json'))

    file.each do |row|
      Itinerary.where(cycle: row[0], segment: row[1]).pluck(:retail).uniq.each do |retail|
        Msl.create(cycle: row[0], segment: row[1], retail: retail, product: row[2])
      end
    end
  end
end
