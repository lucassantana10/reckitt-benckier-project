class CreateSpreadsheets < ActiveRecord::Migration
  def change
    create_table :spreadsheets do |t|
      enable_extension 'hstore'

      t.attachment :file
      t.hstore :issues
      t.boolean :success
      t.string :source_type
      t.timestamp :processed_at
      t.timestamp :deleted_at

      t.timestamps null: false
    end
  end
end
