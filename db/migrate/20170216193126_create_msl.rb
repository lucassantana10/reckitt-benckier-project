class CreateMsl < ActiveRecord::Migration
  def change
    create_table :msls do |t|
      t.string :cycle
      t.string :segment
      t.string :retail
      t.string :product
    end
  end
end
