class CreateItinerariesUsers < ActiveRecord::Migration
  def change
    create_table :itineraries_users do |t|
      t.belongs_to :itinerary, index: true
      t.belongs_to :user, index: true
    end
  end
end
