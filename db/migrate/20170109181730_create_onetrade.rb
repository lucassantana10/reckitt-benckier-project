class CreateOnetrade < ActiveRecord::Migration
  def change
    create_table :onetrade do |t|
      t.integer :ot_id
      t.integer :visit_id
      t.datetime :execution_date_time
      t.datetime :answered_at
      t.string :store
      t.string :code
      t.string :document
      t.string :channel
      t.string :retail
      t.string :pos_flag
      t.string :shopping
      t.string :postal_code
      t.string :address
      t.string :address_type
      t.string :neighborhood
      t.string :city
      t.string :state_code
      t.string :region
      t.string :longitude
      t.string :latitude
      t.string :form_name
      t.string :question
      t.string :option
      t.string :answer
      t.string :points
      t.string :indication
      t.string :category
      t.string :brand
      t.string :brand_2
      t.string :cycle
      t.datetime :week_date
      t.datetime :week_date_end
      t.datetime :work_start
      t.datetime :work_end
      t.datetime :rest_start
      t.datetime :rest_end
      t.integer :item_id
      t.string :product
      t.string :product_category
      t.string :classification
      t.string :fabricante
      t.string :segmento

      t.timestamps null: false
    end
  end
end
