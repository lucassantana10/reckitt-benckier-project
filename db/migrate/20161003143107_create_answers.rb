class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.references :itinerary, index: true, foreign_key: true
      t.references :question, index: true, foreign_key: true
      t.references :item, index: true, foreign_key: true
      t.text :title
      t.text :product
      t.text :indication
      t.timestamp :answered_at

      t.timestamps null: false
    end
  end
end
