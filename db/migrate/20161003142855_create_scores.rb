class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.references :itinerary, index: true, foreign_key: true
      t.references :kpi, index: true, foreign_key: true
      t.decimal :points

      t.timestamps null: false
    end
  end
end
