class CreateItineraries < ActiveRecord::Migration
  def change
    create_table :itineraries do |t|
      t.string :pos_tax_code, index: true
      t.integer :visit_id, index: true
      t.datetime :execution_date_time
      t.string :store
      t.string :retail
      t.string :shopping
      t.string :postal_code
      t.string :address
      t.string :neighborhood
      t.string :city
      t.string :state_code
      t.string :region
      t.decimal :latitude, precision: 10, scale: 6
      t.decimal :longitude, precision: 10, scale: 6
      t.date :week_date
      t.string :cycle
      t.string :segment
      t.string :economic_group
      t.string :area_manager

      t.datetime :deleted_at
      t.timestamps null: false
    end
  end
end
