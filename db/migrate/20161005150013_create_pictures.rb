class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.references :item, index: true, foreign_key: true
      t.references :itinerary, index: true, foreign_key: true
      t.string :url
      t.string :title
      t.string :indication
      t.string :category
      t.timestamp :deleted_at

      t.timestamps null: false
    end
    add_index :pictures, [:indication, :category]
  end
end
