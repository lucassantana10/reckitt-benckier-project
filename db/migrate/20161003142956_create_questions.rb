class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :form, index: true, foreign_key: true
      t.text :title
      t.timestamp :deleted_at

      t.timestamps null: false
    end
  end
end
