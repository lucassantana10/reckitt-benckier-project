class CreateKpis < ActiveRecord::Migration
  def change
    create_table :kpis do |t|
      t.string :name
      t.integer :total_points

      t.timestamp :deleted_at
      t.timestamps null: false
    end
  end
end
