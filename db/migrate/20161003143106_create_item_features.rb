class CreateItemFeatures < ActiveRecord::Migration
  def change
    create_table :item_features do |t|
      t.references :item, index: true, foreign_key: true
      t.string :category
      t.string :subcategory
      t.string :manufacturer
      t.string :brand
      t.string :brand_2
      t.string :sku

      t.timestamp :deleted_at
      t.timestamps null: false
    end
  end
end
