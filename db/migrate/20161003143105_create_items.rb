class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.references :itinerary, index: true, foreign_key: true
      t.string :key

      t.timestamp :deleted_at
      t.timestamps null: false
    end
    add_index :items, :key
  end
end
