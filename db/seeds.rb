# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Role.find_or_create_by(:name => 'Administrator')

@user = User.find_by(email: "desenvolvimento@trackingtrade.com.br")
if @user.nil?
   User.create(:name => "Administrador", :email => "desenvolvimento@trackingtrade.com.br", :password => "@trackingtrade2014reckitt", :password_confirmation => "@trackingtrade2014reckitt", :role_id => 1 )
end

@user = AdminUser.find_by(email: "admin@example.com")
if @user.nil?
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
end
