# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170216193126) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "answers", force: :cascade do |t|
    t.integer  "itinerary_id"
    t.integer  "question_id"
    t.integer  "item_id"
    t.text     "title"
    t.text     "product"
    t.text     "indication"
    t.datetime "answered_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "answers", ["item_id"], name: "index_answers_on_item_id", using: :btree
  add_index "answers", ["itinerary_id"], name: "index_answers_on_itinerary_id", using: :btree
  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree

  create_table "forms", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "item_features", force: :cascade do |t|
    t.integer  "item_id"
    t.string   "category"
    t.string   "subcategory"
    t.string   "manufacturer"
    t.string   "brand"
    t.string   "brand_2"
    t.string   "sku"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "item_features", ["item_id"], name: "index_item_features_on_item_id", using: :btree

  create_table "items", force: :cascade do |t|
    t.integer  "itinerary_id"
    t.string   "key"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "items", ["itinerary_id"], name: "index_items_on_itinerary_id", using: :btree
  add_index "items", ["key"], name: "index_items_on_key", using: :btree

  create_table "itineraries", force: :cascade do |t|
    t.string   "pos_tax_code"
    t.integer  "visit_id"
    t.datetime "execution_date_time"
    t.string   "store"
    t.string   "retail"
    t.string   "shopping"
    t.string   "postal_code"
    t.string   "address"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "state_code"
    t.string   "region"
    t.decimal  "latitude",            precision: 10, scale: 6
    t.decimal  "longitude",           precision: 10, scale: 6
    t.date     "week_date"
    t.string   "cycle"
    t.string   "segment"
    t.string   "economic_group"
    t.string   "area_manager"
    t.datetime "deleted_at"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "itineraries", ["pos_tax_code"], name: "index_itineraries_on_pos_tax_code", using: :btree
  add_index "itineraries", ["visit_id"], name: "index_itineraries_on_visit_id", using: :btree

  create_table "itineraries_users", force: :cascade do |t|
    t.integer "itinerary_id"
    t.integer "user_id"
  end

  add_index "itineraries_users", ["itinerary_id"], name: "index_itineraries_users_on_itinerary_id", using: :btree
  add_index "itineraries_users", ["user_id"], name: "index_itineraries_users_on_user_id", using: :btree

  create_table "kpis", force: :cascade do |t|
    t.string   "name"
    t.integer  "total_points"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "msls", force: :cascade do |t|
    t.string "cycle"
    t.string "segment"
    t.string "retail"
    t.string "product"
  end

  create_table "onetrade", force: :cascade do |t|
    t.integer  "ot_id"
    t.integer  "visit_id"
    t.datetime "execution_date_time"
    t.datetime "answered_at"
    t.string   "store"
    t.string   "code"
    t.string   "document"
    t.string   "channel"
    t.string   "retail"
    t.string   "pos_flag"
    t.string   "shopping"
    t.string   "postal_code"
    t.string   "address"
    t.string   "address_type"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "state_code"
    t.string   "region"
    t.string   "longitude"
    t.string   "latitude"
    t.string   "form_name"
    t.string   "question"
    t.string   "option"
    t.string   "answer"
    t.string   "points"
    t.string   "indication"
    t.string   "category"
    t.string   "brand"
    t.string   "brand_2"
    t.string   "cycle"
    t.datetime "week_date"
    t.datetime "week_date_end"
    t.datetime "work_start"
    t.datetime "work_end"
    t.datetime "rest_start"
    t.datetime "rest_end"
    t.integer  "item_id"
    t.string   "product"
    t.string   "product_category"
    t.string   "classification"
    t.string   "fabricante"
    t.string   "segmento"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "pictures", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "itinerary_id"
    t.string   "url"
    t.string   "title"
    t.string   "indication"
    t.string   "category"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "pictures", ["indication", "category"], name: "index_pictures_on_indication_and_category", using: :btree
  add_index "pictures", ["item_id"], name: "index_pictures_on_item_id", using: :btree
  add_index "pictures", ["itinerary_id"], name: "index_pictures_on_itinerary_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.integer  "form_id"
    t.text     "title"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "questions", ["form_id"], name: "index_questions_on_form_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "scores", force: :cascade do |t|
    t.integer  "itinerary_id"
    t.integer  "kpi_id"
    t.decimal  "points"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "scores", ["itinerary_id"], name: "index_scores_on_itinerary_id", using: :btree
  add_index "scores", ["kpi_id"], name: "index_scores_on_kpi_id", using: :btree

  create_table "spreadsheets", force: :cascade do |t|
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.hstore   "issues"
    t.boolean  "success"
    t.string   "source_type"
    t.datetime "processed_at"
    t.datetime "deleted_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "name"
    t.integer  "role_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "answers", "items"
  add_foreign_key "answers", "itineraries"
  add_foreign_key "answers", "questions"
  add_foreign_key "item_features", "items"
  add_foreign_key "items", "itineraries"
  add_foreign_key "pictures", "items"
  add_foreign_key "pictures", "itineraries"
  add_foreign_key "questions", "forms"
  add_foreign_key "scores", "itineraries"
  add_foreign_key "scores", "kpis"
end
