base_url = ENV["ONETRADE_BASE_URL"] || 'https://onetrade-droyde.herokuapp.com'
token = ENV["ONETRADE_TOKEN"] || "c5a6625ce195f43f209437b51ffcc3a62cef8eb3d0c9ab21158bcd82ba7d7a14"
campaign_id = ENV["ONETRADE_CAMPAIGN_ID"] || '10'

Rails.application.config.onetrade = { base_url: base_url, token: token, campaign_id: campaign_id }
